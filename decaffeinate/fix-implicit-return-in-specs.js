function isLastStatement(path) {
  const returnStatement = path.value;
  if (!returnStatement || !returnStatement.argument) {
    return false;
  }
  const functionBlock = path.parent.value;
  const lastFunctionStatement = functionBlock.body.slice(-1);
  return lastFunctionStatement[0] === path.value;
}


export default (fileInfo, api) => {
  const j = api.jscodeshift;
  const root = j(fileInfo.source);

  root
    .find(j.CallExpression, {
      callee: {
        type: 'Identifier',
        name: 'describe',
      },
    })
    .find(j.ReturnStatement)
    .filter(isLastStatement)
    .replaceWith((path) => {
      if (path.node.argument) {
        return j.expressionStatement(path.node.argument);
      }
      return j(path).toSource();
    });
  return root.toSource();
};
